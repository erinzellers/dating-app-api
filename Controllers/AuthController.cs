using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DatingApp.API.Data;
using DatingApp.API.DTOs;
using DatingApp.API.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IAuthRepository _repo;
        public AuthController(IAuthRepository repo, IConfiguration config)
        {
            _config = config;
            _repo = repo;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserForRegisterDto userForRegisterDto)
        {
            if(!string.IsNullOrEmpty(userForRegisterDto.Username))
                userForRegisterDto.Username = userForRegisterDto.Username.ToLower();

            //check to see if username is already taken
            if(await _repo.UserExists(userForRegisterDto.Username))
                //if true add error to model state for return to client
               ModelState.AddModelError("Username", "Username already exists");

            //validate request
            if(!ModelState.IsValid)
                // modelstate is invalid, return any errors to client
                return BadRequest(ModelState);           
           
            //instansiate user object
            var userToCreate = new User
            {
                Username = userForRegisterDto.Username
            };

            var createdUser = _repo.Register(userToCreate, userForRegisterDto.Password);

            return StatusCode(201);
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]UserForLoginDto userForLoginDto)
        {
            //get the user from the repo login method otherwise return null
            var userFromRepo = await _repo.Login(userForLoginDto.Username.ToLower(), userForLoginDto.Password);

            //check if user exists against db
            if(userFromRepo == null)
                return Unauthorized();

            //if user exists generate token handler
            var tokenHandler = new JwtSecurityTokenHandler();

            //key to sign our token to allow server validation
            var key = Encoding.ASCII.GetBytes(_config.GetSection("AppSettings:Token").Value);

            //describes our token and what's inside it
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    //specifies the id of the user
                    new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                    new Claim(ClaimTypes.Name, userFromRepo.Username)                    
                }),
                //when the token expires
                Expires = DateTime.Now.AddDays(1),
                //encodes the key in byte array form
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), 
                SecurityAlgorithms.HmacSha512Signature)
            };
            //actually creates the token then pass in the descriptor
            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            //finally write the token out
            var tokenString = tokenHandler.WriteToken(token);

            //pass the token to the client
            return Ok(new {tokenString});
        }
    }
}