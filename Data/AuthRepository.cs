using System;
using System.Threading.Tasks;
using DatingApp.API.models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        public AuthRepository(DataContext context)
        {
            _context = context;

        }
        public async Task<User> Login(string username, string password)
        {
            //find user values from db that match that username and assign those values to the user variable
            //else var user gets value of null
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            if (user == null)
                return null;

            //returns a bool, if PW isn't verified then null is returned
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            //auth success!
            return user;
        }

        private bool VerifyPasswordHash(string password, byte[] storedPasswordHash, byte[] storedPasswordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedPasswordSalt))
            {               
                //hash and salt password input value
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                //check stored hash and salt against the newly input password

                if(computedHash.Length != storedPasswordHash.Length) return false;

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if(computedHash[i] != storedPasswordHash[i]) return false;
                }
            }
            return true;
        }

        public async Task<User> Register(User user, string password)
        {
            //we'll pass a reference to these to our method with "out"
            byte[] passwordHash, passwordSalt;

            //hash and new salt for new users password 
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            //store generated pasword and salt in DB
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                //use hmac to create random key which will be used as the password salt
                passwordSalt = hmac.Key;

                //using the password string which after being converted to a byte array is then hashed
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public async Task<bool> UserExists(string username)
        {
            if(await _context.Users.AnyAsync(x => x.Username == username))
                return true;

            return false;
        }
    }
}